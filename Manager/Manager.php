<?php

/*
 * Copyright @ David Salbei
 */

/**
 * Description of Manager
 *
 * @author David Salbei
 */

namespace Incolab\DBALServiceProvider\Manager;

use Doctrine\DBAL\Connection;


abstract class Manager {

    protected $dbal;

    public function __construct(Connection $dbal) {
        $this->dbal = $dbal;
    }

}
