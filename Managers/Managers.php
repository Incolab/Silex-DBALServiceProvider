<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incolab\DBALServiceProvider\Managers;

/**
 * Description of Managers
 *
 * @author david
 */
class Managers {

    private $conn;
    private $managers;

    public function __construct(\Doctrine\DBAL\Connection $conn) {
        $this->conn = $conn;
        $this->managers = [];
    }

    /**
     * @param string $entity_name
     * @return EntityRepository
     */
    public function getManager($entity_name) {
        $hashedKey = hash("md5", $entity_name);

        if (!isset($this->managers[$hashedKey])) {
            $bKeys = explode(":", $entity_name);
            $class = "\\" . $bKeys[0] . "\\Manager\\" . $bKeys[1] . "Manager";
            $this->managers[$hashedKey] = new $class($this->conn);
        }
        return $this->managers[$hashedKey];
    }

}
