<?php

namespace Incolab\DBALServiceProvider\Service;

use Silex\Application;
use Pimple\ServiceProviderInterface;
use Incolab\DBALServiceProvider\Managers\Managers;

class DBALManagerServiceProvider implements ServiceProviderInterface
{
    private $managers;
    
    public function __construct(\Doctrine\DBAL\Connection $conn) {
        $this->managers = new Managers($conn);
    }

    

    /**
     * Create a config key in the app storing the configuration in an array
     *
     * @param Application $app
     */
    public function register(\Pimple\Container $app)
    {   
        $app["dbal"] = function ($app) {
            return $this->managers;
        };
    }

    public function boot(Application $app)
    {
        
    }
}
